const Webpack = require("webpack");
const Glob = require("glob");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ManifestPlugin = require("webpack-manifest-plugin");
const CleanObsoleteChunks = require("webpack-clean-obsolete-chunks");
const TerserPlugin = require("terser-webpack-plugin");
const LiveReloadPlugin = require("webpack-livereload-plugin");

const path = require("path");

const mode = process.env.NODE_ENV || "development";
const prod = mode === "production";
const configurator = {
    entries: function() {
        var entries = {
            application: [
                './node_modules/jquery-ujs/src/rails.js',
                './assets/css/application.scss',

            ],
        }

        Glob.sync("./assets/*/*.*").forEach((entry) => {
            if (entry === './assets/css/application.scss') {
                return
            }

            let key = entry.replace(/(\.\/assets\/(src|js|css|go)\/)|\.(ts|js|s[ac]ss|go)/g, '')
            if (key.startsWith("_") || (/(ts|js|s[ac]ss|go)$/i).test(entry) == false) {
                return
            }

            if (entries[key] == null) {
                entries[key] = [entry]
                return
            }

            entries[key].push(entry)
        })
        return entries
    },

    plugins() {
        var plugins = [
            new Webpack.ProvidePlugin({ $: "jquery", jQuery: "jquery" }),
            new MiniCssExtractPlugin({ filename: "[name].[contenthash].css" }),
            new CopyWebpackPlugin([{ from: "./assets", to: "" }], { copyUnmodified: true, ignore: ["css/**", "js/**", "src/**"] }),
            new Webpack.LoaderOptionsPlugin({ minimize: true, debug: false }),
            new ManifestPlugin({ fileName: "manifest.json" }),
            new CleanObsoleteChunks()
        ];

        return plugins
    },

    moduleOptions: function() {
        return {
            rules: [{
                    test: /\.s[ac]ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        { loader: "css-loader", options: { sourceMap: true } },
                        { loader: "postcss-loader", options: { sourceMap: true } },
                        { loader: "sass-loader", options: { sourceMap: true } }
                    ]
                },
                { test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/ },
                { test: /\.jsx?$/, loader: "babel-loader", exclude: /node_modules/ },
                { test: /\.(woff|woff2|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/, use: "url-loader" },
                { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: "file-loader" },
                { test: require.resolve("jquery"), use: "expose-loader?jQuery!expose-loader?$" },
                { test: /\.go$/, use: "gopherjs-loader" }
            ]
        }
    },

    buildConfig: function() {
        // NOTE: If you are having issues with this not being set "properly", make
        // sure your GO_ENV is set properly as `buffalo build` overrides NODE_ENV
        // with whatever GO_ENV is set to or "development".
        const env = process.env.NODE_ENV || "development";

        var config = {
            mode: env,
            entry: configurator.entries(),
            output: { filename: "[name].[hash].js", path: `${__dirname}/public/assets` },
            plugins: configurator.plugins(),
            module: configurator.moduleOptions(),
            resolve: {
                extensions: ['.ts', '.js', '.json']
            }
        }

        if (env === "development") {
            config.plugins.push(new LiveReloadPlugin({ appendScriptTag: true }))
            return config
        }

        const terser = new TerserPlugin({
            terserOptions: {
                compress: {},
                mangle: {
                    keep_fnames: true
                },
                output: {
                    comments: false,
                },
            },
            extractComments: false,
        })

        config.optimization = {
            minimizer: [terser]
        }

        return config
    }
}


//module.exports = configurator.buildConfig()

module.exports = {
    entry: {
        bundle: ["./assets/js/index.js"]
    },
    resolve: {
        alias: {
            svelte: path.resolve("node_modules", "svelte")
        },
        extensions: [".mjs", ".js", ".svelte"],
        mainFields: ["svelte", "browser", "module", "main"]
    },
    output: {
        path: __dirname + "/public/assets",
        filename: "[name].js",
        chunkFilename: "[name].[id].js"
    },
    module: {
        rules: [{
                test: /\.svelte$/,
                use: {
                    loader: "svelte-loader",
                    options: {
                        emitCss: true,
                        hotReload: true
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    /**
                     * MiniCssExtractPlugin doesn't support HMR.
                     * For developing, use 'style-loader' instead.
                     * */
                    prod ? MiniCssExtractPlugin.loader : "style-loader",
                    "css-loader"
                ]
            }
        ]
    },
    mode,
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css"
        })
    ],
    devtool: prod ? false : "source-map"
};